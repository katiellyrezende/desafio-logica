function getDados(){
var table = document.getElementById("tb_fatura") ;
var dados = [];
var value = 0;
var month = 0;

    for (var i =1; i < table.rows.length; i++) {   
        
        value= table.rows[i].cells[1].innerHTML;
        month= table.rows[i].cells[2].innerHTML;

        value = value.replace(".", "").replace(",", ".").replace("R$ ", "");
        
        dados.push({
            month: month,
            value: value,
        });    
    }

    dados.sort(function (a, b){
        return  a.month<b.month?-1:1;
    });
    prepareConsolidated(dados)
}

function prepareConsolidated(dados){
var aux;
var consolidado=[];

    dados.forEach((dados) => {
    if (dados.month !== aux) {
        consolidado.push({
        month: dados.month,
        value: parseFloat(dados.value),
        });
    } else {
        consolidado.forEach((consolidado) => {
        if (consolidado.month === aux) {
            consolidado.value = parseFloat(consolidado.value) + parseFloat(dados.value); 
        }
    });
}
    aux = dados.month;
});
setDados(consolidado);
}

function setDados(consolidado){
var body = document.getElementsByTagName("body")[0];
var title = document.createElement("h3");
var table = document.createElement("table");
var tHead =  document.createElement("tHead");
var tBody = document.createElement("tbody");
var rowCol = document.createElement("tr"); 
var col1 = document.createElement("th");
var col2 = document.createElement("th");
var titleText = document.createTextNode("Consolidado");
var colText1 = document.createTextNode("Mês");
var colText2 = document.createTextNode("Total Gasto")

table.className = "table"
col1.appendChild(colText1);
col2.appendChild(colText2);
rowCol.appendChild(col1);
rowCol.appendChild(col2);
tHead.appendChild(rowCol);
title.appendChild(titleText);

    consolidado.forEach((consolidado) => {
        var row = document.createElement("tr"); 
        var cell1 = document.createElement("td");
        var cell2 = document.createElement("td");

        var cellText1 = document.createTextNode(returnMonth(consolidado.month));
        var cellText2 = document.createTextNode("R$ " + consolidado.value.toLocaleString("pt-BR"));

        cell1.appendChild(cellText1);
        cell2.appendChild(cellText2)
        row.appendChild(cell1);
        row.appendChild(cell2);
        tBody.appendChild(row);
    });
    
table.appendChild(tHead);
table.appendChild(tBody);
body.appendChild(title);
body.appendChild(table);
}
 
function returnMonth(month){
    switch (month){
        case "01":  month = "Janeiro";
            break;
        case "02" : month =  "Fevereiro";
            break;
        case "03" : month = "Março";
            break;
        case "04" : month = "Abril";
            break;
        case "05" : month =  "Maio";
            break;
        case "06" : month = "Junho";
            break;
        case "07" : month = "Julho";
            break;
        case "08" : month =  "Agosto";
            break;
        case "09" : month =  "Setembro";
            break;
        case "10" : month = "Outubro";
            break;
        case "11" : month =  "Novembro";
            break;
        case "12" : month =  "Dezembro";
            break;
    }          
return month;
} 